# README #

Please create automated tests using Selenium and Page Object Model pattern to perform the following steps (for a browser of your choice). 

1. Open https://www.ultimateqa.com/filling-out-forms/
2. Fill out the form on the right, but intentionally enter -1 as a result of addition
3. Submit the form and confirm that the numbers have changed
4. Close the browser

1. Open https://www.ultimateqa.com/filling-out-forms/
2. Fill out the form on the right, fill in the correct number
3. Submit the form and confirm that a 'Success' message is displayed. 
4. Close the browser

Please apply best practices you know.
Send us the solution as a .zip archive or send us a link to your repo. We prefer a repo with your commit history :) 
