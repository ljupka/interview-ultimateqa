﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace InterviewUltimateqaLibrary.Repository
{
    public class FormRepo
    {
        [FindsBy(How = How.Id, Using = "et_pb_contact_form_1")]
        protected internal IWebElement Form { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='et_pb_contact_form_1']//button")]
        protected internal IWebElement SubmitButtonRight { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".input.et_pb_contact_captcha")]
        protected internal IWebElement InputResult { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".et_pb_contact_captcha_question")]
        protected internal IWebElement CaptchaNumbers { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@id='et_pb_contact_name_1']")]
        protected internal IWebElement InputNameRight { get; set; }

        [FindsBy(How = How.XPath, Using = "//textarea[@id='et_pb_contact_message_1']")]
        protected internal IWebElement InputMessageRight { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='et_pb_contact_form_1']/div/p")]
        protected internal IWebElement SuccessMessageAfterSubmitAction { get; set; }
    }
}
