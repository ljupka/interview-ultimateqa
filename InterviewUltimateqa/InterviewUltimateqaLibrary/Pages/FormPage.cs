﻿using InterviewUltimateqaLibrary.Config;
using InterviewUltimateqaLibrary.Repository;
using InterviewUltimateqaLibrary.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;

namespace InterviewUltimateqaLibrary.Pages
{
    public class FormPage : FormRepo

    {
        public FormPage()
        {
            PageFactory.InitElements(Driver.driver, this);
        }

        public bool IsAt()
        {
            try { return Form.Displayed; }
            catch { return false; }
        }

        public FormPage EnterName(String name)
        {
            Actions.Enter(InputNameRight, name);
            return this;
        }

        public FormPage EnterMessage(String message)
        {
            Actions.Enter(InputMessageRight, message);
            return this;
        }

        public FormPage EntertIncorrectResult(int num)
        {
            string initialCaptchaNumbers = CaptchaNumbers.Text;
            Session.InitialCaptchaNumbers = initialCaptchaNumbers;

            string number = num.ToString();
            Actions.Enter(InputResult, number);
            return this;
        }

        public FormPage SubmitForm()
        {
            Actions.Click(SubmitButtonRight);
            return this;
        }

        public bool AreNumbersChanged()
        {
            return !CaptchaNumbers.Text.Equals(Session.InitialCaptchaNumbers);            
        }

        public int ParseAndAddCaptchaNumbers()
        {
            string initialCaptchaNumbers = CaptchaNumbers.Text;
            string[] numberStrings = initialCaptchaNumbers.Split('+');
            int firstNumber = int.Parse(numberStrings[0]);
            int secondNumber = int.Parse(numberStrings[1]);
            return firstNumber + secondNumber;
        }

        public FormPage EnterCorrectResult()
        {
            string result = ParseAndAddCaptchaNumbers().ToString();
            Actions.Enter(InputResult, result);
            return this;
        }

        public bool IsSuccessMessageDisplayed()
        {
            try
            {
                return Actions.WaitForElement(SuccessMessageAfterSubmitAction);
            }
            catch 
            {
                return false;
            }
        }
        
    }
}
