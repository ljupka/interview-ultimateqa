﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InterviewUltimateqaTests
{
    [TestClass]
    public class FormTest : TestBase
    {
        [TestMethod]
        public void FormIncorrectInput()
        {
            Assert.IsTrue(FormPage.IsAt(), "Form page is not opened!");
            FormPage.EnterName("Ljubica").EnterMessage("Test").EntertIncorrectResult(-1).SubmitForm();
            Assert.IsTrue(FormPage.AreNumbersChanged(), "Numbers are not changed");
        }

        [TestMethod]
        public void FormCorrectInput()
        {
            Assert.IsTrue(FormPage.IsAt(), "Form page is not opened!");
            FormPage.EnterName("Ljubica").EnterMessage("Test").EnterCorrectResult().SubmitForm();
            Assert.IsTrue(FormPage.IsSuccessMessageDisplayed(), "Success message is not displayed!");
        }

    }
}
